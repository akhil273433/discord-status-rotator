const Discord = require("discord.js-selfbot-v13");
require("dotenv").config();

const TOKENS = [
  process.env.nickToken1,
  process.env.nickToken2,
  // process.env.nickToken3,
  // process.env.nickToken4,
  // Add more tokens if needed
];

// Function to setup RPC and restart the program in a 1-hour interval
function setupRPCAndRestart() {
  setupRPC().then(() => {
    // Restart the program in 1 hour
    setTimeout(setupRPCAndRestart, 1 * 60 * 60 * 1000); // 1 hour in milliseconds
  });
}

async function setupRPC() {
  for (const TOKEN of TOKENS) {
    try {
      await Presence(TOKEN);
    } catch (error) {
      console.error(`Error setting up RPC for token ${TOKEN}:`, error);
    }
  }
}

async function Presence(TOKEN) {
  const client = new Discord.Client({ readyStatus: false, checkUpdate: false });

  const applicationId = process.env.applicationId;
  const type = "PLAYING";
  const url = "https://discord.gg/nickstore";
  const name = "𝗡𝗜𝗖𝗞 𝗦𝗧𝗢𝗥𝗘";
  const details =
    "Nitro, Boosts, Profile Decorations, OTT Subscriptions & Other Services @ Low Price";
  const largeImage =
    "https://media.discordapp.net/attachments/1163022210932428860/1188790462337798154/creavite_proficon_nick_store.gif?ex=659bce64&is=65895964&hm=c7da5ffa2ac1dbb6fc020aae4206b7dff1172bc27a2e9db52f964ddf35fd437a&=&width=320&height=320";
  const largeImageText = "Join Server For More Details";
  const smallImage =
    "https://cdn.discordapp.com/emojis/785233158801981521.gif?size=128&quality=lossless";
  const smallImageText = "Fully Safe & Trusted";
  const button1Label = "Discord Server";
  const button1Url = "https://discord.gg/nickstore";
  const button2Label = "Website";
  const button2Url = "https://nick-store.akhiljohns.site";

  return new Promise((resolve, reject) => {
    client.on("ready", async () => {
      try {
        const guild = client.guilds.cache.get(process.env.serverId);
        const memberCount = guild ? guild.memberCount : 0;

        const rpc = new Discord.RichPresence()
          .setApplicationId(applicationId)
          .setType(type)
          .setURL(url)
          .setName(name)
          .setDetails(details)
          .setAssetsLargeImage(largeImage)
          .setAssetsLargeText(largeImageText)
          .setAssetsSmallImage(smallImage)
          .setAssetsSmallText(smallImageText)
          .addButton(button1Label, button1Url)
          .addButton(button2Label, button2Url)
          .setState(`${memberCount}/100 Members`);

        await client.user.setActivity(rpc);
        console.log(`RPC setup successful for token ${TOKEN}`);
        // Set the bot's status to "dnd" after setting up the RPC
        client.user.setStatus("dnd");
        resolve();
      } catch (error) {
        console.error(`Error setting up RPC for token ${TOKEN}:`, error);
        reject(error);
      }
    });

    client.login(TOKEN);
  });
}

// Start the program
setupRPCAndRestart();

// Export the setupRPCAndRestart function
module.exports = setupRPCAndRestart;
