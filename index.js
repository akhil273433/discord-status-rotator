const Discord = require("discord.js-selfbot-v13");
require("dotenv").config();
const fs = require("fs");
const client = new Discord.Client({
  readyStatus: false,
  checkUpdate: false,
});
const keepAlive = require("./server");
// const setupRPCAndRestart = require("./rpc");

const statuses = JSON.parse(fs.readFileSync("./statuses.json"));
let currentIndex = 0;
const TOKENS = [
  process.env.nickToken1,
  // process.env.nickToken2,
  // process.env.nickToken3,
  // process.env.nickToken4,
  // Add more tokens if needed
];

client.on("ready", async () => {
  console.clear();
  // Wait for the client to be fully ready
  await client.guilds.fetch(); // Fetch guilds to ensure emojis are loaded
  console.log("All guilds fetched.");

  // Check emojis for all statuses
  await Promise.all(statuses.map(checkEmoji));
  console.log("All emojis checked.");

  // Setup RPC and restart
  // await setupRPCAndRestart();
  // console.log("RPC setup and restart complete.");

  // Start the status cycle
  setInterval(cycleStatus, 2000); // Adjust the interval as needed
  cycleStatus();
  console.log(
    `✅ @${client.user.username} - Rich Presence Successfully Activated`
  );
});

// Function to check if the emoji is available
async function checkEmoji(status) {
  if (!status.emoji) return; // If no emoji, return
  try {
    await client.emojis.cache.get(status.emoji); // Try to get the emoji
  } catch (error) {
    console.error(`Emoji ${status.emoji} is not available.`);
    status.emoji = null; // Set emoji to null if not available
  }
}

function cycleStatus() {
  const currentStatus = statuses[currentIndex];
  const final = new Discord.CustomStatus()
    .setState(currentStatus.state)
    .setEmoji(currentStatus.emoji);
  client.user.setActivity(final)
  client.user.setStatus("dnd");
  currentIndex = (currentIndex + 1) % statuses.length;
  if (currentIndex === 0) {
    // If the last status is reached, log a message
  }
}

keepAlive();

TOKENS.forEach((TOKEN, index) => {
  const botNumber = index + 1;
  console.log(`Starting bot ${botNumber} with token: ${TOKEN}`);
  client.login(TOKEN);
});
